import Head from 'next/head'
import Carousel from 'react-bootstrap/Carousel'
import Alert from 'react-bootstrap/Alert'
import Card from 'react-bootstrap/Card'

export default function Home() {
  return (
    <div className="container">
     
	   
		

		
		<nav class="navbar navbar-default">
		  <div class="container">
		    
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-icon-collapse" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>

		       
		      <a class="logo" href="#"><img src="img/logo_nav2.png"/></a>

		    </div>

		    
		    <div class="collapse navbar-collapse" id="nav-icon-collapse">
		      
			  
		      <ul class="nav navbar-nav navbar-right">
		        <li><a href="#" data-scroll-nav="0" class="active">Home</a></li>
		        <li><a href="#" data-scroll-nav="1">Nosotros</a></li>
		        <li><a href="#" data-scroll-nav="2">Servicios</a></li>
		        
		        
		        <li><a href="#" data-scroll-nav="3">Demo</a></li>
		        
		        <li><a href="#" data-scroll-nav="4">Contacto</a></li>
		      </ul>
		    </div>
		  </div>
		</nav>
		

		
		<section id="home" class="header" data-scroll-index="0"  data-stellar-background-ratio="0.8">

		<svg class="left-svg" width="50%" height="80" viewBox="0 0 100 102" preserveAspectRatio="none">
            <path d="M0 100 L100 100 L0 10 Z"></path>
        </svg>
        <svg class="right-svg" width="50%" height="80" viewBox="0 0 100 102" preserveAspectRatio="none">
            <path d="M0 100 L100 100 L100 10 Z"></path>
        </svg>

			<div class="v-middle">
				<div class="container">
					<div class="row">

						
						<div class="caption">
							<h5>Covalu S.C.</h5>
							<h1 class="cd-headline clip">
					            <span class="blc">Implementación de</span>
					            <span class="cd-words-wrapper">
					              <b class="is-visible">ERP</b>
					              <b>CRM</b>
					              <b>BI</b>
					              <b>WMS</b>
					              <b>Portal Web</b>
					            </span>
			          		</h1>

			          		
			          		<div class="social-icon">
			          			<a href="https://www.facebook.com/Covalu-SC-436381337123343/" target="_blank">
			          				<span><i class="fa fa-facebook" aria-hidden="true"></i></span>
			          			</a>
			          			<a href="https://www.instagram.com/covalu_sc/?igshid=x44jupj381sb" target="_blank">
			          				<span><i class="fa fa-instagram" aria-hidden="true"></i></span>
			          			</a>
			          			<a href="https://www.linkedin.com/company/covalu-s-c" target="_blank">
			          				<span><i class="fa fa-linkedin" aria-hidden="true"></i></span>
			          			</a>
			          			<a href="https://www.youtube.com/channel/UCeEeunTtohz-BSvDu8Z3yLg" target="_blank">
			          				<span><i class="fa fa-youtube" aria-hidden="true"></i></span>
			          			</a>
			          			<a href="https://wa.me/5215513986065?text=Hola%20info%20Covalu" target="_blank">
			          				<span><i class="fa fa-whatsapp" aria-hidden="true"></i></span>
			          			</a>
			          		</div>
						</div>
						
					</div>
				</div>
			</div>
		</section>
		

		
		<section class="hero section-padding pb-70" data-scroll-index="1">
			<div class="container">
				<div class="row">

					
					<div class="col-md-7">
						<div class="content mb-30">
							<h3>Nosotros</h3> <br/>
							
							<p><b>Covalu</b><br/><br/> Somos un grupo de profesionales enfocado en la asesoría de innovaciones de vanguardia tecnológica ERP, CRM, BI, WMS, entre otros. Contamos con una red de expertos en los más complejos procesos de ultima generación, para lograr soluciones integras con valor agregado para tu empresa.<br/><br/>

							Nos concentramos en entender la realidad, las oportunidades y prioridades de tu negocio. Analizamos estas necesidades, no solo con el enfoque de un sistema, sino como la integración de procesos, gente y tecnología para brindarte justo lo que tu negocio necesita. <br/><br/>

							Contamos con un amplio equipo de profesionales especializados, constantemente capacitados en la implementación de soluciones viables y creativas que te ayudarán a solucionar cualquier situación.</p>

							
							<div class="skills">
								<div class="item">
									<div class="skills-progress">
										<h6>BI</h6>
										<span data-value="100%"></span>
									</div>
								</div>
								<div class="item">
									<div class="skills-progress">
										<h6>Portal Web</h6>
										<span data-value="100%"></span>
									</div>
								</div>
								<div class="item">
									<div class="skills-progress">
										<h6>ERP</h6>
										<span data-value="100%"></span>
									</div>
								</div>
							</div>
							
						</div>
					</div>

					
					<div class="col-md-5">
						<div class="mb-30">
							<img src="img/nosotros.jpg" alt=""/>
						</div>
					</div>


				</div>
			</div>
		</section>
		

		
		<section class="portfolio section-padding pb-70" data-scroll-index="2">
			<div class="container">
				<div class="row">

					
					<div class="section-head">
						<h3>Nuestros Partners</h3>
					</div>

					
					<div class="filtering text-center mb-50">
						<span data-filter='.partner' class="active">Todos</span>
						<span data-filter='.intelisis'>Intelisis</span>
						<span data-filter='.qlik'>Qlik</span>
						<span data-filter='.penta'>Pentaho</span>
					</div>

					
					<div class="gallery text-center">

						
						<div class="col-md-4 col-sm-6 items partner">
							<div class="item-img">
								<img src="img/portfolio/1.jpg" alt="image"/>
								<div class="item-img-overlay">
									<div class="overlay-info v-middle text-center">
										<h6 class="sm-titl">INTELISIS ERP<br/><br/>Integra, coordina y automatiza todas las áreas y los procesos de tu negocio. Se adapta al giro de tu empresa o negocio.</h6>
										<div class="icons">
											<span class="icon link">
												<a href="img/portfolio/1.jpg" title="Integra, coordina y automatiza todas las áreas y los procesos de tu negocio. Se adapta al giro de tu empresa o negocio.">
													<i class="fa fa-search-plus" aria-hidden="true"></i>
												</a>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						
						<div class="col-md-4 col-sm-6 items partner">
							<div class="item-img">
								<img src="img/portfolio/2.jpg" alt="image"/>
								<div class="item-img-overlay">
									<div class="overlay-info v-middle text-center">
										<h6 class="sm-titl">QLIK BI<br/><br/>Te permite convertir datos en conocimiento; busca y analiza visualmente la información de tu empresa o negocio.</h6>
										<div class="icons">
											<span class="icon link">
												<a href="img/portfolio/2.jpg" title="Te permite convertir datos en conocimiento, busca, analiza visualmente la información de tu empresa o negocio.">
													<i class="fa fa-search-plus" aria-hidden="true"></i>
												</a>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						
						<div class="col-md-4 col-sm-6 items partner penta">
							<div class="item-img">
								<img src="img/portfolio/3.jpg" alt="image"/>
								<div class="item-img-overlay">
									<div class="overlay-info v-middle text-center">
										<h6 class="sm-titl">PENTAHO ETL<br/><br/>Es una herramienta de extracción de datos. Transforma estos datos y cárgalos en otro sitio.</h6>
										<div class="icons">
											<span class="icon link">
												<a href="img/portfolio/3.jpg" title="Es una herramienta de extracción de datos. Transforma estos datos y cargalos en otro sitio.">
													<i class="fa fa-search-plus" aria-hidden="true"></i>
												</a>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						
						<div class="col-md-4 col-sm-6 items qlik">
							<div class="item-img">
								<img src="img/portfolio/QLIK/QlikNP.jpg" alt="image"/>
								<div class="item-img-overlay">
									<div class="overlay-info v-middle text-center">
										<h6 class="sm-titl">QlikNPrinting<br/><br/>Le permite crear informes a partir de sus documentos de Qlikview.</h6>
										<div class="icons">
											<span class="icon link">
												<a href="img/portfolio/QLIK/QlikNP.jpg" title="Le permite crear informes a partir de sus documentos de Qlikview">
													<i class="fa fa-search-plus" aria-hidden="true"></i>
												</a>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						
						<div class="col-md-4 col-sm-6 items qlik">
							<div class="item-img">
								<img src="img/portfolio/QLIK/QlikSense.jpg" alt="image"/>
								<div class="item-img-overlay">
									<div class="overlay-info v-middle text-center">
										<h6 class="sm-titl">QlikSense<br/><br/>Busque y explore, libremente, entre todos sus datos y cambie de forma instantánea los análisis cuando surjan ideas nuevas.</h6>
										<div class="icons">
											<span class="icon link">
												<a href="img/portfolio/QLIK/QlikSense.jpg" title="Busque y explore libremente entre todos sus datos y cambie de forma instantánea los análisis cuando surjan ideas nuevas.">
													<i class="fa fa-search-plus" aria-hidden="true"></i>
												</a>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						
						<div class="col-md-4 col-sm-6 items qlik">
							<div class="item-img">
								<img src="img/portfolio/QLIK/QlikView.jpg" alt="image"/>
								<div class="item-img-overlay">
									<div class="overlay-info v-middle text-center">
										<h6 class="sm-titl">QlikView<br/><br/>Analiza la información que tu negocio genera y toma decisiones con base en los reportes de tu negocio.</h6>
										<div class="icons">
											<span class="icon link">
												<a href="img/portfolio/QLIK/QlikView.jpg" title="Analiza la información que tu negocio genera y toma decisiones con base en los reportes de tu negocio.">
													<i class="fa fa-search-plus" aria-hidden="true"></i>
												</a>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						
						<div class="col-md-4 col-sm-6 items qlik">
							<div class="item-img">
								<img src="img/portfolio/QLIK/QlikGeo.jpg" alt="image"/>
								<div class="item-img-overlay">
									<div class="overlay-info v-middle text-center">
										<h6 class="sm-titl">QlikGeoAnalytics<br/><br/>Con esta herramienta puede generar mapas que le proporcionen un contexto visual para tomar decisiones correctas en torno a su ubicación.</h6>
										<div class="icons">
											<span class="icon link">
												<a href="img/portfolio/QLIK/QlikGeo.jpg" title="Con esta herramienta puede generar mapas que le proporcionen un contexto visua que le ayuda a tomar decisiones correctas en torno a su ubicación.">
													<i class="fa fa-search-plus" aria-hidden="true"></i>
												</a>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						
						<div class="col-md-4 col-sm-6 items intelisis">
							<div class="item-img">
								<img src="img/portfolio/INTELISIS/Automotriz.jpg" alt="image"/>
								<div class="item-img-overlay">
									<div class="overlay-info v-middle text-center">
										<h6 class="sm-titl">AUTOMOTRIZ<br/><br/>Administración, venta de unidades, post-venta, refacciones.</h6>
										<div class="icons">
											<span class="icon link">
												<a href="img/portfolio/INTELISIS/Automotriz.jpg" title="Administración, venta de unidades, post-venta, refacciones.">
													<i class="fa fa-search-plus" aria-hidden="true"></i>
												</a>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						
						<div class="col-md-4 col-sm-6 items intelisis">
							<div class="item-img">
								<img src="img/portfolio/INTELISIS/Inmobiliaria.jpg" alt="image"/>
								<div class="item-img-overlay">
									<div class="overlay-info v-middle text-center">
										<h6 class="sm-titl">INMOBILIARIA Y CONSTRUCCIÓN<br/><br/>Proyectos, contratos, construcción, administración de inmuebles, recursos humanos.</h6>
										<div class="icons">
											<span class="icon link">
												<a href="img/portfolio/INTELISIS/Inmobiliaria.jpg" title="Proyectos, contratos, construcción, admon. de inmuebles, recursos humanos">
													<i class="fa fa-search-plus" aria-hidden="true"></i>
												</a>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						
						<div class="col-md-4 col-sm-6 items intelisis">
							<div class="item-img">
								<img src="img/portfolio/INTELISIS/manufactura.jpg" alt="image"/>
								<div class="item-img-overlay">
									<div class="overlay-info v-middle text-center">
										<h6 class="sm-titl">MANUFACTURA<br/><br/>Inventario cero, proceso WIP, stock, productividad, mantenimiento preventivo.</h6>
										<div class="icons">
											<span class="icon link">
												<a href="img/portfolio/INTELISIS/manufactura.jpg" title="Inventario cero, proceso WIP, stock, productividad, mantenimiento preventivo.">
													<i class="fa fa-search-plus" aria-hidden="true"></i>
												</a>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						
						<div class="col-md-4 col-sm-6 items intelisis">
							<div class="item-img">
								<img src="img/portfolio/INTELISIS/mayoristas.jpg" alt="image"/>
								<div class="item-img-overlay">
									<div class="overlay-info v-middle text-center">
										<h6 class="sm-titl">COMERCIO MAYORISTA<br/><br/>Facturación, embarques, inventarios, compras nacionales y de importación.</h6>
										<div class="icons">
											<span class="icon link">
												<a href="img/portfolio/INTELISIS/mayoristas.jpg" title="Facturación, embarques, inventarios, compras nacionales y de importación.">
													<i class="fa fa-search-plus" aria-hidden="true"></i>
												</a>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						
						<div class="col-md-4 col-sm-6 items intelisis">
							<div class="item-img">
								<img src="img/portfolio/INTELISIS/minorista.jpg" alt="image"/>
								<div class="item-img-overlay">
									<div class="overlay-info v-middle text-center">
										<h6 class="sm-titl">COMERCIO MINORISTA<br/><br/>Distribución, POS, pedidos, reportes.</h6>
										<div class="icons">
											<span class="icon link">
												<a href="img/portfolio/INTELISIS/minorista.jpg" title="Distribución, POS, pedidos, reportes.">
													<i class="fa fa-search-plus" aria-hidden="true"></i>
												</a>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						
						<div class="col-md-4 col-sm-6 items intelisis">
							<div class="item-img">
								<img src="img/portfolio/INTELISIS/RH.jpg" alt="image"/>
								<div class="item-img-overlay">
									<div class="overlay-info v-middle text-center">
										<h6 class="sm-titl">SERVICIOS PROFESIONALES<br/><br/>Recursos humanos, tiempos y materiales, inventarios, proyectos, gastos, administración.</h6>
										<div class="icons">
											<span class="icon link">
												<a href="img/portfolio/INTELISIS/RH.jpg" title="Recursos humanos, tiempos y materiales, inventarios, proyectos, gastos, administración.">
													<i class="fa fa-search-plus" aria-hidden="true"></i>
												</a>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>
		

		
		<section class="services section-padding bg-gray text-center pb-70">
			<div class="container">
				<div class="row">

					
					<div class="section-head">
						<h3>Servicios</h3>
					</div>

					
					<div class="col-md-4">
						<div class="item">
							<span class="icon"><i class="fa fa-file" aria-hidden="true"></i></span>
							<h6>Consultoría</h6>
							<p>Ofrecemos un servicio de consultoría en tecnologías de la información, siempre adaptándonos a las necesidades de tu negocio.</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="item">
							<span class="icon"><i class="fa fa-users" aria-hidden="true"></i></span>
							<h6>Outsourcing</h6>
							<p>Cubrimos tus necesidades de capital humano. Nos enfocamos en los requerimentos de cada uno de nuestros clientes.</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="item">
							<span class="icon"><i class="fa fa-cogs" aria-hidden="true"></i></span>
							<h6>Soporte</h6>
							<p>Realizamos soporte en las herramientas que ofrecen nuestros partners Intelisis, Qlik y Pentaho.<br/><br/></p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="item">
							<span class="icon"><i class="fa fa-terminal" aria-hidden="true"></i></span>
							<h6>Desarrollo Intelisis</h6>
							<p>Contamos con un amplio equipo de expertos en desarrollo e implementación de software en Intelisis ERP.</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="item">
							<span class="icon"><i class="fa fa-laptop" aria-hidden="true"></i></span>
							<h6>Desarrollo Web</h6>
							<p>Desarrollamos en diferentes lenguajes, JAVA, PHP, C++.<br/><br/></p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="item">
							<span class="icon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
							<h6>Desarrollo Móvil</h6>
							<p>Generamos desarrollos móviles para iOS y Android.<br/><br/></p>
						</div>
					</div>
				</div>
			</div>
		</section>
		


		<section class="contact section-padding" data-scroll-index="3">
			<div class="container">
				<div class="row">

					
					<div class="section-head">
						<h3>Contáctanos</h3>
					</div>
						
						<form action="mail.php" method='post' class='form' id='contact-form'>
		                    <div class="messages"></div>

		                    <div class="controls">

		                        <div class="row">
		                            <div class="col-md-6">
		                                <div class="form-group">
		                                    <input id="form_name" type="text" name="name" placeholder="Nombre" required="required"/>
		                                </div>
		                            </div>

		                             <div class="col-md-6">
		                                <div class="form-group">
		                                    <input id="form_email" type="email" name="email" placeholder="Email" required="required"/>
		                                </div>
		                            </div>
		                        </div>

		                        <div class="row">
		                            <div class="col-md-6">
		                                <div class="form-group">
		                                    <input id="form_name" type="text" name="tel" placeholder="Teléfono" required="required"/>
		                                </div>
		                            </div>

		                            <div class="col-md-6">
		                                <div class="form-group">
											<select name="category" type="text" required> 
												<option value="default" disabled selected>Categoría</option>
												<option>Intelisis ERP</option>
												<option>Qlik</option>
												<option>Pentahoe</option>
												<option>Otro</option>
											</select>
		                                </div>
		                            </div>
		                        </div>

		                        <div class="row">
		                            <div class="col-md-12">
		                                <div class="form-group">
		                                    <textarea id="form_message" name="message" placeholder="Mensaje" rows="4" required="required"></textarea>
		                                </div>

		                                <input type="submit" value="Enviar" class="buton buton-bg"/>
		                                <input type="reset" value="Borrar" class="buton buton-bg"/>
		                            </div>
		                        </div>
		                    </div>
		                </form>
				</div>
			</div>
		</section>

		
		<section class="contact" data-scroll-index="4">
			<div class="container">
				<div class="row">

					<div class="col-md-offset-1 col-md-10">

						
						<div class="info text-center mb-50">

							<div class="col-md-3">
								<div class="item">
									<span class="icon"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
									<h6>Dirección</h6>
									<p>Manuel J. Othón #226 Int.2 Col. Tránsito Del. Cuauhtémoc C.P. 06820 Ciudad de México</p>
								</div>
							</div>

							<div class="col-md-3">
								<div class="item">
									<span class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
									<h6>Email</h6>
									<p>covalu@covalu.com</p>
								</div>
							</div>

							<div class="col-md-3">
								<div class="item">
									<span class="icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
									<h6>Teléfonos</h6>
									<p>
										(55) 7825-7142 <br/>
										(55) 4362-2664 <br/>
										(55) 2538-6534
									</p>
								</div>
							</div>

							<div class="col-md-3">
								<div class="item">
									<span class="icon"><i class="fa fa-whatsapp" aria-hidden="true"></i></span>
									<h6><a href="https://wa.me/5215513986065?text=Hola%20info%20Covalu" target="_blank">Whatsapp</a></h6>
									<p>
									
									</p>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>

		            <div class="row">
		                <div class="col-md-12 padding">

							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1881.47690510055!2d-99.13253158318177!3d19.41440159752638!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1fec3a01cc22b%3A0xfb7cb896f8fe4a2!2sInt.2%2C+Manuel+Jos%C3%A9+Oth%C3%B3n+226%2C+Tr%C3%A1nsito%2C+06820+Ciudad+de+M%C3%A9xico%2C+CDMX!5e0!3m2!1ses!2smx!4v1561873501884!5m2!1ses!2smx" width="400" height="200" frameborder="0"  allowfullscreen></iframe>
		                            	
		                </div>
		            </div>

				</div>
			</div>
		</section>
		

		<br/><br/>

		
		<footer>
			<div class="container text-center">
				<div class="row">
					<p>Copyright &copy; Covalu S.C. 2018</p>
				</div>
			</div>
		</footer>
		
       </div>
  )
}
